# Roots Boots!

How to style a website in 15 mins.

_Go from zero..._
<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-a.html>

_...to hero..._
<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-o.html>

_...in 15 mins._

# What is bootstrap?

It is a CSS and Javascript framework to help style websites and add interactivity. You don't need to know anything more than basic HTML to start using it.

Frameworks are good::

- They provide a consistent look and feel.
- They save time.
- They are well documented for 
  - you,
  - the other people on your team,
  - the next person who works on your website.
- Millions of people use it, free tech support! 

This is not a competitor to angular or react. But it can be used with them.

## Opinionated

Bootstrap is very opinionated.

- Go with the flow. It does a lot for you, but arguing with it does not go well.
- Every time you want to do something search for "How do i _____ in Bootstrap" before hacking away. Someone probably has already asked the question and you will have multiple solutions to choose from.


# GETTING STARTED

Install options:

- Adding precompiled local source files
- Adding via CDN (cached version that will load faster) 
- Using NPM or Bower (we won't go over those)


We will use the CDN option and borrow it from the starter template from here: 

<https://getbootstrap.com/docs/4.0/getting-started/introduction/#starter-template>

Added some content.

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-a.html>

## BAM!

Looks better already!

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-b.html>


### But wait there is more!

- Layout
- Typography
- Components
- Utitlites
- And more!


## LAYOUT: 

### Responsive Grid 
Containers (columns) and rows

<https://getbootstrap.com/docs/4.0/layout/grid/>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-c.html>

### Utilities
Spacing (padding and margin)

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-d.html>

## CONTENT

### Typography

Sets some nice defaults, as you saw.

Typography and utilities

<https://getbootstrap.com/docs/4.0/content/typography/>

#### Lead

<https://getbootstrap.com/docs/4.0/content/typography/#lead>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-e.html>

#### Muted text

<https://getbootstrap.com/docs/4.0/content/typography/#customizing-headings>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-e.html>

### Images

Alignment

<https://getbootstrap.com/docs/4.0/content/images/#aligning-images>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-e.html>


## COMPONENTS

### Alerts

<https://getbootstrap.com/docs/4.0/components/alerts/>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-f.html>


### Jumbotron

Hero callout space

<https://getbootstrap.com/docs/4.0/components/jumbotron/>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-g.html>

_Clearfix Utility_

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-h.html>


### Buttons

<https://getbootstrap.com/docs/4.0/components/buttons/>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-i.html>


### List group

<https://getbootstrap.com/docs/4.0/components/list-group/>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-j.html>

### Forms

<https://getbootstrap.com/docs/4.0/components/forms/>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-k.html>

### Cards

<https://getbootstrap.com/docs/4.0/components/card/>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-m.html>

### Navbar crazy town

Adding Javascript to the page now.
- Basic nav
- Navbar
- Branding
- Dropdown nav
<https://getbootstrap.com/docs/4.0/components/navbar/>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-n.html>

## Themeing 

### CSS Variables

<https://getbootstrap.com/docs/4.0/getting-started/theming/#css-variables>

<https://jhc36.pages.oit.duke.edu/roots_boots/html/index-o.html>

### Theme roller

Go crazy! 
<https://bootstrap.build/>

# Go impress your freinds

